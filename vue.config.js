module.exports = {
  css: {
    extract: false,
    loaderOptions: {
      sass: {
        prependData: `
          @import "src/global/custom-variables.scss";
          @import "src/global/base-style.scss";
        `,
      },
    },
  },
};
