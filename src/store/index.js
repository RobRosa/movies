import Vue from 'vue';
import Vuex from 'vuex';
import MoviesModule from './modules/movies';
import TMDB from '@/api/TMDB';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    appConfigLoaded: null,
    appConfigData: null,
  },
  mutations: {
    setAppConfigLoaded(state, payload) {
      state.appConfigLoaded = payload.hasConfigLoadSucceed;
    },
    setAppConfigData(state, payload) {
      state.appConfigData = payload.appConfigData;
    },
  },
  actions: {
    loadAppConfig() {
      let hasSucceededToLoadConfig = null;

      TMDB.loadConfigurationData()
        .then((appConfigData) => {
          hasSucceededToLoadConfig = true;
          this.commit('setAppConfigData', { appConfigData });
        })
        .catch(() => {
          hasSucceededToLoadConfig = false;
        })
        .finally(() => {
          this.commit('setAppConfigLoaded', {
            hasConfigLoadSucceed: hasSucceededToLoadConfig,
          });
        });
    },
  },
  getters: {
    getAppImagesLocation(state) {
      if (!state.appConfigData) {
        return '';
      }

      return state.appConfigData.images.secure_base_url;
    },
    getPosterSizes(state) {
      const objPosterSizes = {};

      if (state.appConfigData) {
        state.appConfigData.images.poster_sizes.forEach((size) => {
          objPosterSizes[size] = size;
        });
      }

      return objPosterSizes;
    },
  },
  modules: {
    movies: MoviesModule,
  },
});
