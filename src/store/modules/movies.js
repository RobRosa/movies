import TMDB from '@/api/TMDB';

export default {
  namespaced: true,
  state: {
    newestMovies: [],
    newestMoviesCurrentPage: 1,
    newestMoviesTotalPages: 0,
    favoriteMovies: {},
  },
  mutations: {
    setNewestMovies(state, payload) {
      state.newestMovies = [
        ...state.newestMovies,
        ...payload.newestMovies,
      ];
    },
    setNewestMoviesTotalPages(state, payload) {
      state.newestMoviesTotalPages = payload.totalPages;
    },
    setNewestMoviesCurrentPage(state, payload) {
      state.newestMoviesCurrentPage = payload.newestMoviesCurrentPage;
    },
    setFavoriteMovies(state, payload) {
      state.favoriteMovies = { ...payload.objFavoriteMovies };
      localStorage.setItem('FAVORITE_MOVIES', JSON.stringify(payload.objFavoriteMovies));
    },
  },
  actions: {
    loadNewestMovies({ commit }, page = 1) {
      TMDB.fetchNewestMovies(page)
        .then((movies) => {
          commit('setNewestMovies', { newestMovies: movies.results });
          commit('setNewestMoviesCurrentPage', { newestMoviesCurrentPage: page });
          commit('setNewestMoviesTotalPages', { totalPages: movies.total_pages });
        });
    },
    loadFavoriteMovies({ commit }) {
      const localFavoriteMovies = localStorage.getItem('FAVORITE_MOVIES');
      if (localFavoriteMovies) {
        commit('setFavoriteMovies', { objFavoriteMovies: JSON.parse(localFavoriteMovies) });
      }
    },
    addFavoriteMovie({ state, commit }, payload) {
      const { movieId, posterPath, title } = payload;
      const objFavoriteMovies = state.favoriteMovies;

      objFavoriteMovies[movieId] = {
        id: movieId,
        poster_path: posterPath,
        title,
      };

      commit('setFavoriteMovies', { objFavoriteMovies });
    },
    removeFavoriteMovie({ state, commit }, payload) {
      const objFavoriteMovies = state.favoriteMovies;
      delete objFavoriteMovies[payload.movieId];
      commit('setFavoriteMovies', { objFavoriteMovies });
    },
  },
};
