import Moment from 'moment';
import TMDBEndpoints from './TMDBEndpoints';
import RequestHandler from './RequestHandler';

export default {
  /**
   * Formats the requests to send to the API.
   * @returns Promise
   */
  TMDBApiRequest(path, objParameters) {
    const baseApiUrl = process.env.VUE_APP_MOVIES_API_BASE_URL;
    const apiVersion = process.env.VUE_APP_MOVIES_API_VERSION;
    const apiKey = process.env.VUE_APP_MOVIES_API_KEY;

    const normalizedPath = path[0] === '/'
      ? path
      : `/${path}`;

    const endpoint = `${baseApiUrl}/${apiVersion}${normalizedPath}`;

    return RequestHandler.get(endpoint, {
      api_key: apiKey,
      ...objParameters,
    });
  },

  /**
   * Loads the app configuration data.
   * @returns Promise
   */
  loadConfigurationData() {
    const localConfigurationData = localStorage.getItem('TMDB_APP_CONFIG');

    if (localConfigurationData) {
      const parsedConfigData = JSON.parse(localConfigurationData);
      const currentDate = new Moment();
      const lastConfigUpdate = new Moment(parsedConfigData.last_update);

      if (currentDate.diff(lastConfigUpdate, 'days') < 3) {
        return Promise.resolve(parsedConfigData.data);
      }
    }

    return this.fetchConfigurationData();
  },

  /**
   * Request the app configuration data for the API and saves it locally.
   * @returns Promise
   */
  async fetchConfigurationData() {
    const configurationData = await this.TMDBApiRequest(TMDBEndpoints.GET_APP_CONFIGURATION);

    localStorage.setItem('TMDB_APP_CONFIG', JSON.stringify({
      last_update: (new Moment()).format('YYYY-MM-DD'),
      data: configurationData,
    }));

    return Promise.resolve(configurationData);
  },

  /**
   * Request the most recent movies available.
   * @returns Promise
   */
  fetchNewestMovies(page = 1) {
    return this.TMDBApiRequest(TMDBEndpoints.DISCOVER_MOVIES, {
      sort_by: 'primary_release_date.desc',
      'primary_release_date.lte': (new Moment()).format('YYYY-MM-DD'),
      page,
    });
  },

  /**
   * Request detailed information about a movie.
   * @returns Promise
   */
  fetchMovieDetails(movieId) {
    return this.TMDBApiRequest(`${TMDBEndpoints.GET_MOVIE_INFORMATION}${movieId}`);
  },
};
