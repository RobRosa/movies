export default {
  GET_APP_CONFIGURATION: '/configuration',
  DISCOVER_MOVIES: '/discover/movie',
  GET_MOVIE_INFORMATION: '/movie/',
};
