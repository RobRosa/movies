const RequestHandler = {
  /**
   * Effectively makes the requests.
   * @returns Promise
   */
  async request(endpoint, options = {}) {
    const response = await fetch(endpoint, options);

    if (response.ok) {
      return response.json();
    }

    throw new Error(`Error in request. Resource: ${endpoint}`);
  },

  /**
   * Creates a GET request to the given endpoint with the specified query parameters
   * @returns Promise
   */
  get(endpoint, queryParameters) {
    const paramsString = queryParameters
      ? `?${(new URLSearchParams(queryParameters)).toString()}`
      : '';

    return RequestHandler.request(`${endpoint}${paramsString}`);
  },
};

export default RequestHandler;
