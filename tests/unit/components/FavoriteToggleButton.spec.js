import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import FavoriteToggleButton from '@/components/FavoriteToggleButton.vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('FavoriteToggleButton.vue', () => {
  let actions;
  let state;
  let store;

  const favoriteMovie = {
    id: 1234,
    poster_path: 'xyz.jpg',
    title: 'a favorite movie',
  };

  const notFavoriteMovie = {
    id: 123,
    poster_path: 'abc.jpg',
    title: 'a movie',
  };

  beforeEach(() => {
    state = {
      favoriteMovies: {
        '1234': favoriteMovie,
      },
    };

    actions = {
      addFavoriteMovie: jest.fn(),
      removeFavoriteMovie: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        movies: {
          state,
          actions,
          namespaced: true,
        }
      }
    })
  });

  it('Renders an outlined icon when movie is not among the favorites', () => {
    const wrapper = shallowMount(FavoriteToggleButton, {
      store,
      localVue,
      propsData: {
        movieInfo: notFavoriteMovie,
      }
    });

    expect(wrapper.find('.favorite-button--favorited').exists()).toBe(false);
  });
  
  it('Renders a filled icon when movie is among the favorites', () => {
    const wrapper = shallowMount(FavoriteToggleButton, {
      store,
      localVue,
      propsData: {
        movieInfo: favoriteMovie,
      }
    });
    
    expect(wrapper.find('.favorite-button--favorited').exists()).toBe(true);
  });

  it('Calls store action "addFavoriteMovie" when button is clicked and the movie is not a favorite yet', () => {
    const wrapper = shallowMount(FavoriteToggleButton, {
      store,
      localVue,
      propsData: {
        movieInfo: notFavoriteMovie,
      },
    });
    
    const button = wrapper.find('.favorite-button');
    button.trigger('click');
    expect(actions.addFavoriteMovie).toHaveBeenCalled();
  });
  
  it('Calls store action "removeFavoriteMovie" when button is clicked and the movie is a favorite', () => {
    const wrapper = shallowMount(FavoriteToggleButton, {
      store,
      localVue,
      propsData: {
        movieInfo: favoriteMovie,
      },
    });
    
    const button = wrapper.find('.favorite-button');
    button.trigger('click');
    expect(actions.removeFavoriteMovie).toHaveBeenCalled();
  });
});
