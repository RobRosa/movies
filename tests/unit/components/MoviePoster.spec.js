import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import MoviePoster from '@/components/MoviePoster.vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('MoviePoster.vue', () => {
  let getters;
  let store;

  beforeEach(() => {
    getters = {
      getAppImagesLocation: () => 'https://image.tmdb.org/t/p/',
      getPosterSizes: () => ({
        original: "original",
        w154: "w154",
        w185: "w185",
        w342: "w342",
        w500: "w500",
        w780: "w780",
        w92: "w92",
      }),
    }

    store = new Vuex.Store({
      getters,
    });
  });

  it('Renders a default movie poster when none is received', () => {
    const wrapper = shallowMount(MoviePoster, {
      propsData: {
        path: null,
      },
    });
    expect(wrapper.find('.movie-poster__image').element.src).toEqual('http://localhost/default-movie-poster.png');
  });

  it('Renders a movie poster when one is received', () => {
    const wrapper = shallowMount(MoviePoster, {
      store,
      localVue,
      propsData: {
        path: '/xyz.png',
      },
    });
    expect(wrapper.find('.movie-poster__image').element.src).toMatch('https://image.tmdb.org/t/p/w342/xyz.png');
  });
});
