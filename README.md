# movies

Esta aplicação consome dados da Api TMDB, para o correto funcionamento da aplicação é necessário [criar um conta no site](https://www.themoviedb.org/signup) e em seguida gerar uma chave de API [neste link](https://www.themoviedb.org/settings/api).

## Project setup
Instalação de dependências:
```
npm install
```

## Configuração do arquivo de ambiente do projeto:
 1. Crie uma cópia do arquivo .env.example;
 2. Renomeie o novo arquivo criado para .env;
 3. Substitua o placeholder {{ API_KEY_HERE }} com a chave de API (v3 auth) gerada no site do TMDB.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
